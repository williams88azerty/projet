FROM node:12-alpine
COPY . /app
WORKDIR /home/osboxes/Dockerfile/
RUN npm install
EXPOSE 8000
CMD ["npm", "start"]

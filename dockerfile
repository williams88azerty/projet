FROM node:12-alpine
COPY . /home/osboxes/
WORKDIR /home/osboxes/
RUN npm i
EXPOSE 8000
CMD ["npm", "start"]
